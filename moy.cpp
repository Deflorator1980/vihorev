// SimPearson.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <time.h>
//#include "Ix\CPP\src\cpplinq\linq.hpp"
#include "boolinq.h"

using namespace std;


double sim_pearson(map<string, map<string, double>> prefs, string person, string other)
{
    return static_cast<double>(time(0));
}


//vector<pair<double, string>> top_matches(map<string, vector<double>> prefs, string person, unsigned len = 5)
vector<pair<double, string>> top_matches(map<string, map<string, double>> prefs, string person, unsigned len = 5)
{
    auto result =
            boolinq::from(prefs)
                    .where  ( [&](auto x) {return x.first != person;} )
                    .select ( [&](auto x) {return make_pair(sim_pearson(prefs, person, x.first), x.first);} )
                    .orderBy( [](auto x) {return x.first;} )
                    .reverse()
                    .take(len)
                    .toVector();

    return result;
}

///*

//vector<pair<double,string>> top_matches_old( map<string, vector<double>> prefs, string person, unsigned len = 5)
//vector<pair<double, string>> top_matches(map<string, map<string, double>> prefs, string person, unsigned len = 5){
//	vector<pair<double,string>> result;
//
//	for (auto& x : prefs)
//	{
//		if (x.first != person)
//			result.emplace_back(sim_pearson(prefs, person, x.first), x.first);
//	}
//	std::sort( begin(result), end(result), [](auto x, auto y) {return x.first > y.first;});
//	result.resize(len);
//    std::cout << sim_pearson(prefs,0,0);
//	return result;
//}


int main()
{
//    map<string, double> m1 = {{"hui", 1}, {"pisda", 2}};
//    map<string, double> m1a = {{"hui", 11}, {"pisda", 21}};
//    map<string, map> m2 = {{"anus", m1}, {"huyanus", m1a}};
//    top_matches(m2, "3424");
//    unsigned  char [] a = {2, 6, -2, 1, 7};
    int a[] = { 2, 6, -2, 1, 7 };
    std::reverse(begin(a), end(a));
    for_each( begin(a), end(a), [](auto x){cout << x << " ";} );
//    std::cout << "COOL";
    return 0;
}

